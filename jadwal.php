<html>
<title>CRUD</title>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body class="list-group-item list-group-item-info">
		<nav>
				<div class="alert alert-primary" role="alert">	
				<div class="container">
				<a href="index.php" class="btn btn-outline-success my-2 my-sm-0">CRUD</a>
				</div>
				</div>
		</nav>
		<div class="list-group list-group-item ">
			<div class="row list-group-item list-group-item-primary">
				<div class="col-md-8 col-md-offset-2 ">
		<!-- pengaturan style conten judul -->
<div class="container" style="margin-top:8%">
	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<p>
				<center>
					<h5 class="list-group-item active">Jadwal Kelas</h5><hr>
				</center>
			</p>
			<br>
			<p>
			<!-- pembuatan tabel dengan menggunakan class css -->
				<a class="btn btn-primary" href="tambah_jadwal.php">Tambah</a>
			</p>
			<table class="table form-control-sm ">
				<tr>
					<th>
						No 
					</th>
					<th>
						ID Dosen
					</th>
					<th>
						ID Kelas
					</th>
					<th>
						Jadwal
					</th>
                    <th>
						Matakuliah
					</th>
				</tr>
				<!-- sql terhubung ke database -->
					<?php
						include"jalur.php";
						$no = 1;
						$data = mysqli_query ($jalur, " select 
																id_jadwal,
																id_dosen,
																id_kelas,
																jadwal,
                                                                matakuliah
														  from 
														  jadwal_kelas
														  order by id_jadwal DESC");
						while ($row = mysqli_fetch_array ($data))
						{
					?>
				<!-- pemanggialan variabel dan menampilkan -->
				<tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<?php echo $row['id_dosen']; ?>
					</td>
					<td>
						<?php echo $row['id_kelas']; ?>
					</td>
					<td>
						<?php echo $row['jadwal']; ?>
					</td>
                    <td>
						<?php echo $row['matakuliah']; ?>
					</td>
					<!-- button hapus dan edit yang mengarahkan kelas yang telah dicantumkan -->
					<td>
						<a class="btn green" href="editjad.php?id=<?php echo $row['id_jadwal']; ?>">Edit</a> 
						<a class="btn red" href="deljad.php?id=<?php echo $row['id_jadwal']; ?>">Hapus</a>
					</td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>
	</div>
</div>
</body>
</html>