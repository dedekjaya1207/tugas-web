<html>
<title>CRUD</title>
<head>
<!-- pencantuman link css yang digunakan -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body class="list-group-item list-group-item-info">
<nav>
		<div class="alert alert-primary" role="alert">	
        <div class="container">
          <a href="index.php" class="btn btn-outline-success my-2 my-sm-0">CRUD</a>
        </div>
        </div>
</nav>
<div class="list-group list-group-item ">
	<div class="row list-group-item list-group-item-primary">
		<div class="col-md-8 col-md-offset-2 ">
		<!-- pengaturan style conten judul --> 
			<p>
				<center>
					<h5 class="list-group-item active">Data Jadwal</h5><hr>
				</center>
			</p>
			<br>
			<!-- meiliki navigasi yang dibuat agar menuju halaman yang diterakan -->
			<p>
				<a class="btn btn-outline-success my-2 my-sm-0" href="dosen.php">Edit Data Dosen</a>
                <a class="btn btn-outline-success my-2 my-sm-0" href="kelas.php">Edit Data Kelas</a>
                <a class="btn btn-outline-success my-2 my-sm-0" href="jadwal.php">Edit Data Jadwal Kelas</a>
			</p>
			<!-- Penginputan Tabel -->
			<table class="table form-control-sm ">
				<tr>
					<th>
						No 
					</th>
					<th>
						Nama Dosen
					</th>
                    <th>
						Jadwal
					</th>
                    <th>
						Matakuliah
					</th>
					<th>
						Kelas
					</th>
					<th>
						Prodi
					</th>
					<th>
						Fakultas
					</th>
				</tr>
					<!-- SQL menggabungkan 3 buah tabel dengan INNER JOIN -->
					<?php
						include"jalur.php";
						$no = 1;
                        $sql= "SELECT * FROM dosen INNER JOIN jadwal_kelas ON dosen.id_dosen=jadwal_kelas.id_dosen 
						INNER JOIN kelas ON kelas.id_kelas=jadwal_kelas.id_kelas";
						$data = mysqli_query ($jalur, $sql);
						while ($row = mysqli_fetch_array ($data)){
                    ?>
                     <tr>
					 <!-- memanggil variabel dan menampilkannya -->
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<?php echo $row['nama_dosen']; ?>
					</td>
					<td>
						<?php echo $row['jadwal']; ?>
					</td>
					<td>
						<?php echo $row['matakuliah']; ?>
					</td>
					<td>
						<?php echo $row['nama_kelas']; ?>
					</td>
                    <td>
						<?php echo $row['prodi']; ?>
					</td>
					<td>
						<?php echo $row['fakultas']; ?>
					</td>
				</tr>   
				
				<?php
					}
				?>
			</table>
		</div>
	</div>
</div>
</body>
</html>