<html>
<title>CRUD</title>
<!-- pencantuman link css yang digunakan -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body class="list-group-item list-group-item-info">
<nav>
		<div class="alert alert-primary" role="alert">	
        <div class="container">
          <a href="index.php" class="btn btn-outline-success my-2 my-sm-0">CRUD</a>
        </div>
        </div>
</nav>
<div class="list-group list-group-item ">
	<div class="row list-group-item list-group-item-primary">
		<div class="col-md-8 col-md-offset-2 ">
		<!-- pengaturan style conten judul --> 
			<p>
				<center>
					<h5 class="list-group-item active">Tambah</h5><hr>
				</center>
			</p>
                <br>
                <!-- pembuatan form dengan menggunakan class css -->
                <form role="form" method="post" action="inputjad.php">
                    <div class="form-group">
                        <label>ID Dosen</label>
                        <input class="file-control" name="id_dosen">
                    </div>
                    <div class="form-group">
                        <label>ID Kelas</label>
                        <input class="form-control" name="id_kelas">
                    </div>
                    
                    <div class="form-group">
                        <label>Jadwal</label>
                        <input class="form-control" type="date" name="jadwal">
                    </div>
                    <div class="form-group">
                        <label>Matakuliah</label>
                        <input class="form-control" name="matakuliah">
                    </div>
                    <!-- submit yang diarahkan ke jadwal.php -->
                    <button type="submit" class="btn green">Simpan</button>
                    <a href="jadwal.php" class="btn red" style="margin-right:1%;">Batal</a>
                </form>
            </div>
        </div>
        <p>
    </div>
      <!-- pemanggilan json -->
    <script src="style/materialize.min.js"></script>
           
</body>

</html>