<html>
<title>CRUD</title>
<!-- pencantuman link css yang digunakan -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body class="list-group-item list-group-item-info">
<nav>
		<div class="alert alert-primary" role="alert">	
        <div class="container">
          <a href="index.php" class="btn btn-outline-success my-2 my-sm-0">CRUD</a>
        </div>
        </div>
</nav>
<div class="list-group list-group-item ">
	<div class="row list-group-item list-group-item-primary">
		<div class="col-md-8 col-md-offset-2 ">
		<!-- pengaturan style conten judul --> 
			<p>
				<center>
					<h5 class="list-group-item active">EDIT</h5><hr>
				</center>
			</p>
    <?php
	include"jalur.php";
	$no = 1;
	$data = mysqli_query ($jalur, " select 
											id_dosen,
											foto_dosen,
											nip_dosen,
											nama_dosen,
											prodi,
                                            fakultas
									  from 
									  dosen 
									  where id_dosen = $_GET[id]");
	$row = mysqli_fetch_array ($data);
	
?>
    <div class="container" style="margin-top:8%">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>
                    <center>
                        <h5>Edit Data <?= $row['nama_dosen'] ; ?></h5>
                        <hr>
                    </center>
                </p>
                <br>

                <form role="form" method="post" action="updatedosen.php">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="hidden" name="id_dosen" value="<?php echo $row['id_dosen'] ; ?>">
                        <input class="form-control"type ="file" name="foto" value="<?php echo $row['foto_dosen'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>NIP</label>
                        <input class="form-control" name="nip_dosen" value="<?php echo $row['nip_dosen'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>NAMA</label>
                        <input class="form-control" name="nama_dosen" value="<?php echo $row['nama_dosen'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Prodi</label>
                        <input class="form-control" name="prodi" value="<?php echo $row['prodi'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Fakultas</label>
                        <input class="form-control" name="fakultas" value="<?php echo $row['fakultas'] ; ?>">
                    </div>
                    <button type="submit" class="btn green">Perbarui</button>
                    <a href="dosen.php" class="btn red" style="margin-right:1%;">Batal</a>
                </form>
            </div>
        </div>
    </div>
    <script src="style/materialize.min.js"></script>
</body>

</html>