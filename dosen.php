<html>
<title>CRUD</title>
<head>
<!-- pencantuman link css yang digunakan -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body class="list-group-item list-group-item-info">
<nav>
		<div class="alert alert-primary" role="alert">	
        <div class="container">
          <a href="index.php" class="btn btn-outline-success my-2 my-sm-0">CRUD</a>
        </div>
        </div>
</nav>
<div class="list-group list-group-item ">
	<div class="row list-group-item list-group-item-primary">
		<div class="col-md-8 col-md-offset-2 ">
		<!-- pengaturan style conten judul --> 
			<p>
				<center>
					<h5 class="list-group-item active">Data Dosen</h5><hr>
				</center>
			</p>
			<br>
			<!-- pembuatan tabel dengan menggunakan style class css -->
			<p>
				<a class="btn btn-primary" href="tambahdosen.php">Tambah</a>
			</p>
			<table class="table form-control-sm ">
				<tr>
					<th>
						ID Dosen
					</th>
					<th>
						Foto Dosen 
					</th>
					<th>
						NIP
					</th>
					<th>
						Nama Dosen
					</th>
					<th>
						Prodi
					</th>
					<th>
						Fakultas
					</th>
				</tr>
				<!-- query menghubungkan ke database  -->
					<?php
						include"jalur.php";
						$no = 1;
						$data = mysqli_query ($jalur, " select 
																id_dosen,
																foto_dosen,
																nip_dosen,
																nama_dosen,
																prodi,
																fakultas
														  from 
														  dosen 
														  order by id_dosen DESC");
						while ($row = mysqli_fetch_array ($data))
						{
					?>
					<!-- mamanggil variabel dan menampilkannya -->
				<tr>
					<td>
						<?php echo $row['id_dosen']; ?>
					</td>
					<td>
						<?php echo $row['foto_dosen']; ?>
					</td>
					<td>
						<?php echo $row['nip_dosen']; ?>
					</td>
					<td>
						<?php echo $row['nama_dosen']; ?>
					</td>
					<td>
						<?php echo $row['prodi']; ?>
					</td>
					<td>
						<?php echo $row['fakultas']; ?>
					</td>
					<td>
					<!-- button hapus dan edit -->
						<a class="btn green" href="editdos.php?id=<?php echo $row['id_dosen']; ?>">Edit</a> 
						<a class="btn red" href="hapus_d.php?id=<?php echo $row['id_dosen']; ?>">Hapus</a>
					</td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>
	</div>
</div>
</body>
</html>